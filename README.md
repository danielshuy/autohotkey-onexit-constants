# OnExit Constants


## Synopsis

A library of constants for AutoHotkey's [OnExit](https://autohotkey.com/docs/commands/OnExit.htm) function/command.

## Usage

Download the source code and save it to a file, then include it in a script using AutoHotkey's [#Include](https://autohotkey.com/docs/commands/_Include.htm).

When calling the **OnExit** function/command, use the constants defined in the OnExitConst class, eg.

```autohotkey
OnExit("ExitFunc", OnExitConst.ON_EXIT_ADD_AFTER)
```